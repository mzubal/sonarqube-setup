# sonarqube-setup
This ansible tooling is used to install the sonarqube.
Prerequisities (not automated):
 * Opened firewall on port 9000
    * On CENTOS 7, this can be done by running:
    ```
    sudo firewall-cmd --permanent --add-port=9000/tcp
    systemctl stop firewalld
    systemctl start firewalld
    ```
 * Installed postgresql version 9.3 or higher (default for CentOS 7 is 9.1 !)
 * Created posgresql user *sonar* with the usual DB password
 * Created database and schema named *sonar*

After the installation, login to sonarqube and change the *admin* user password to the usual password.
Then go to My Account -> Security and generate new Token. You will then have to update that token (and potential ip of new SonarQube) in the [jenkins-common](https://bitbucket.org/errigal/jenkins-common/src/master/) in vars/common.groovy (look for sonar there).